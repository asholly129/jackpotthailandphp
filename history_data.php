<?php 
// MOCK
    class TSTColorType {
    public $id;
    public $title;
    public $color;
    public $sign;
    public $text;
    }
    class Transaction {
        public $type;
        public $time;
        public $number;
    }
    
    $topupType = new TSTColorType();
    $topupType->id = 1;
    $topupType->title = 'เติมเงิน';
    $topupType->color = '#2FA84F';
    $topupType->sign = '+';
    $topupType->text = 'คุณได้เติมเงินเข้ากระเป๋าเงิน';

    $transferType = new TSTColorType();
    $topupType->id = 2;
    $transferType->title = 'โอนเงิน';
    $transferType->color = '#F3AA18';
    $transferType->sign = '*';
    $transferType->text = 'คุณได้โอนเงินไปบัญชี Jackpot thailand';

    $withdrawType = new TSTColorType();
    $topupType->id = 3;
    $withdrawType->title = 'ถอนเงิน';
    $withdrawType->color = '#FC6C6C';
    $withdrawType->sign = '-';
    $withdrawType->text = 'คุณได้ถอนเงินออกจากกระเป๋าเงิน';

    $temp1 = new Transaction();
    $temp1->type = $topupType;
    $temp1->time = '05-06-2020 , 11:00 น.';
    $temp1->number = 200;

    $temp2 = new Transaction();
    $temp2->type = $withdrawType;
    $temp2->time = '05-06-2020 , 11:00 น.';
    $temp2->number = 200;

    $temp3 = new Transaction();
    $temp3->type = $transferType;
    $temp3->time = '05-06-2020 , 11:00 น.';
    $temp3->number = 200;

    function setupArrayAndaShowLists($data) {
        foreach ($data as $temp) {
        // for ( $i = 0; $i < 5; $i++ ) {
            echo '<div class="row" id="history-box">
            <div class="col-6">
            <span class="align-bottom" style="font-size: 25px;"><b>'.$temp->type->title.
            '</b></span>
            </div>
            <div class=" col-6 text-right">
            <span>
                <span style="font-size: 15px;">&nbsp;</span>
                <span class="align-bottom" style="font-size: 12px;">'.$temp->time.'</span>
            </span>
            </div>

            <div class="col-6">
            <span class="align-bottom" style="font-size: 12px;">'.$temp->type->text.'</span>
            </div>
            <div class="col-6 text-right">
            <span class="align-bottom"> <span style="font-size: 18px; color: '.$temp->type->color.'"><b>'.$temp->type->sign.'฿'.number_format($temp->number, 2, ".", ",").'</b></span></span>
            </div>
            <div class=" col-12">
            <hr>
            </div>
        </div>';
        }
    }
              
?>