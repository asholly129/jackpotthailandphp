<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">        
        <link href="index.css" rel="stylesheet">
        <link href="withdraw.css" rel="stylesheet">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">

        <title>Jackpot Thailand</title>
    </head>

    <body>
        <?php 
            include('nav.php'); 
            include 'dialog.php';

            $lists = ['eieie1', 'eieiei2', 'eieiie3'];
            openSelectDialog($lists);

            $title = "ยืนยันโอนเงิน";
            $number = 100000;
            $from = "กระเป๋าเงิน (100,000.00 THB)";
            $to = "JackpotThailand";
            confirmDialog($title, $number, $from, $to);

            $title = "โอนเงินสำเร็จแล้ว";
            $description = 'คุณได้โอนเงินไปยัง jackpotthailand จำนวน '.number_format($number, 2, ".", ",").' เรียบร้อยแล้ว';
            openSuccessDialog($title, $description, "'https://www.google.com'");
            
            $failTitle = "แจ้งเตือน";
            $FailDescription = "กระเป๋าเงินของคุณมียอดเงิน 0.00 THB";
            openFailDialog($failTitle, $FailDescription)
        ?>

        <div class="container">
            <div class="modal fade" id="howtoTransferDialog" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                      <div class="modal-header">
                        <div class="modal-title"><h4>วิธีการเติมเงิน</h4></div>
                      </div>
                      <div class="modal-body">
                          <p>1. ลูกค้าต้องฝากเงินด้วยบัญชีของลูกค้าเท่านั้น หากใช้บัญชีอื่นโอนมาระบบจะไม่เติมเครดิตให้<br>
                            2. ระบุจำนวนเงินฝาก<br>
                            3. โอนเงินเข้าบัญชีของเว็บก่อนทำการยืนยันการโอน หากไม่ทำตามขั้นตอนระบบจะไม่เติมเครดิตให้<br>
                            4. เลือกโปรโมชัน<br> 
                            5. กดยืนยันฝากเงิน<br><br>
                            จากนั้น ระบบกำลังตรวจสอบและจะส่ง SMS แจ้งผลการเติมเงินไปยังมือถือที่ท่านระบุเอาไว้</p>
                      </div>
                      <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-outline-dark" style="width: 50%;" data-dismiss="modal">ปิด</button>
                      </div>
                  </div>
                </div>
              </div>

            <div class="row">
                <div class="col-7 mt-4">
                    <a href="index.php" style="color:black"><span style="font-size: 25px;"><b>< โอนเงิน</b></span></a>
                </div>
                <div class="col-5 mt-4 text-right"><button class="btn btn-dark" data-toggle="modal" data-target="#howtoTransferDialog">วิธีการโอนเงิน</button></div>
            </div>
            <hr>
            
            <div class="row justify-content-center">
                <?php 
                    include 'wallet.php';
                    showWallet();
                ?>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h6 class="mt-4">*จำนวนเงิน</h6>
                    <input type="text" class="form-control" placeholder="ระบุจำนวนเงิน" id="amount">
                    <h6 class="mt-4">*จาก</h6>
                    <input type="text" class="form-control" placeholder="กระเป๋าเงิน (100,00.00 THB)" id="from">
                    <h6 class="mt-4">*ถึง</h6>
                    
                    <div class="rounded border">
                        <div class="btn-group" role="group" style="width:100%">
                        <?php
                            echo '<button type="button" id="selectBtn" class="btn text-left" style="width: 90%">เลือกโอนให้</button>';
                        ?>
                            
                            <button type="button" class="btn dropdown-toggle">
                        </div> 
                    </div>

                </div>
            </div>

            <div class="row justify-content-center mt-4">
                <div class="col-md-8">
                    <div class="row justify-content-between">
                        <div class="col-6">
                            <button class="btn btn-outline-danger btn-block">ยกเลิก</button>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-success btn-block" id="confirmBtn" disabled>ยืนยัน</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
</body>
</html>


<script type="text/javascript">

$(document).ready(function() {
    $("#selectBtn").click(function(event) {
        $('#selectDialog').modal('toggle');    
    });

    $("#confirmBtn").click(function(event) {
        $('#confirmTransferDialog').modal('toggle');    
    });

    $("#confirmTransferBtn").click(function(event) {
        let isSuccess = true
        if (isSuccess) {
            $('#successDialog').modal('toggle');
        } else {
            $('#failDialog').modal('toggle');
        }
        $('#confirmTransferDialog').modal('hide');
    });

    $("#urlButton").click(function(event) {
        var url = $(this).attr('url'); 
        window.open(url); 
        $('#successDialog').modal('hide');
    });

    var isAmountEmpty = true;
    var isFromEmpty = true;
    var isToEmpty = true;

    $("a").click(function() {
        isToEmpty = false
        var value = $(this).attr('value'); 
        $('#selectBtn')[0].innerHTML = value
        $('#confirmBtn').attr('disabled', (isAmountEmpty || isFromEmpty || isToEmpty));
    });

    $('#amount').keyup(function () {
        isAmountEmpty = $(this).val() == ''
        $('#confirmBtn').attr('disabled', (isAmountEmpty || isFromEmpty || isToEmpty));
    });
    
    $('#from').keyup(function () {
        isFromEmpty = $(this).val() == ''
        $('#confirmBtn').attr('disabled', (isAmountEmpty || isFromEmpty || isToEmpty));
    });
});
</script>