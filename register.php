<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="index.css" rel="stylesheet">
        <link href="withdraw.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <title>Jackpot Thailand</title>
    </head>

    <body>
        <?php include('nav.php'); ?>
        <?php 
            $userCode = 'UFADB11101';
            $password = '0908912233';

            echo '
            <div class="modal fade" id="regisSuccessDialog" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <div class="modal-title"><h4 style="color: #2FA84F;"><b>สมัครสมาชิกสำเร็จ</b></h4></div>
                    </div>
                    <div class="modal-body">
                    <p><b>ข้อมูลสมาชิกของท่าน</b></p>
                    <div class="row justify-content-center mb-2">
                        <div class="col-5 text-center">
                            <div class="yellow-box"><b>รหัสสมาชิก</b></div>
                        </div>
                        <div class="col-7"><b>'.$userCode.'</b></div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-5 text-center">
                        <div class="yellow-box"><b>รหัสผ่าน</b></div>
                        </div>
                        <div class="col-7"><b>'.$password.'</b></div>
                    </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-outline-danger" style="width: 45%;" data-dismiss="modal"><b>ปิด</b></button>
                    <button type="button" class="btn btn-primary" style="width: 45%;" onclick="window.location.href=topup.php">เติมเงินตอนนี้</button>
                    </div>
                </div>
            </div>
            </div>
            ';
        ?>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col col-md-8 mt-4">
                    <h2>สมัครสมาชิกใหม่</h2>
                    <hr>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col col-md-6">
                    <h6 class="mt-4">1. ระบุรหัสสมาชิกที่ใช้งาน (UFABETxxx)</h6>
                    <input type="text" class="form-control" placeholder="ระบุรหัสสมาชิก UFABETxxx">
                    <h6 class="mt-4">2. เบอร์มือถือที่ลงทะเบียนกับทางเว็บไซต์</h6>
                    <input type="text" class="form-control" placeholder="ระบุเบอร์มือถือ" id="phoneTextField">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col col-md-6">
                    <button class="btn btn-warning mt-4 float-right" type="button" disabled id="otpButton">รับรหัสทาง sms</button>    
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col col-md-6">
                    <h6 class="mt-4">3. รหัสที่ได้จาก SMS </h6>
                    <input type="text" class="form-control" placeholder="ระบุรหัสที่ได้รับทาง SMS" id="otpCode">
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col col-md-4">
                    <button class="btn btn-success mb-2 fixed-bottom mx-auto d-block btn-block" type="button" 
                    data-toggle="modal" data-target="#regisSuccessDialog" disabled id="confirmBtn">ยืนยันการลงทะเบียน</button>    
                </div>
            </div>
        </div>
    </body>
</html>

<script>
$(document).ready(function() {
    $('#phoneTextField').keyup(function () {
        $('#otpButton').attr('disabled', ($(this).val() == ''));
    });

    $('#otpCode').keyup(function () {
        $('#confirmBtn').attr('disabled', ($(this).val() == ''));
    });
});

</script>
