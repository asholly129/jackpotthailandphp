<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="index.css" rel="stylesheet">
        <link href="withdraw.css" rel="stylesheet">

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet"> 

        <title>Jackpot Thailand</title>
    </head>

    <body>
        <?php 
            include('nav.php'); 
            include 'dialog.php';
            showNotifyDialog();
            $lists = ['eieie1', 'eieiei2', 'eieiie3'];
            openSelectDialog($lists);
            $title = "เติมเงินสำเร็จแล้ว";
            $description = "ระบบกำลังตรวจสอบ และจะส่ง SMS แจ้งผลการเติมเงินไปยังมือถือที่ท่านระบุเอาไว้";
            openSuccessDialog($title, $description, null);
        ?>

        <div class="container">
            <div class="modal fade" id="howtoTopupDialog" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                  <div class="modal-content">
                      <div class="modal-header">
                        <div class="modal-title"><h4>วิธีการโอนเงิน</h4></div>
                      </div>
                      <div class="modal-body">
                          <p>1. ระบุจำนวนเงิน<br>2. เลือกเว็บไซต์ที่ต้องการโอน<br>3. ยืนยันการโอน</p>
                      </div>
                      <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-outline-dark" style="width: 50%;" data-dismiss="modal">ปิด</button>
                      </div>
                  </div>
                </div>
              </div>

            <div class="row">
                <div class="col-7 mt-4">
                    <a href="index.php" style="color:black"><span style="font-size: 25px;"><b>< เติมเงินอัตโนมัติ</b></span></a>
                </div>
                <div class="col-5 mt-4 text-right"><button type="button" class="btn btn-dark" data-toggle="modal" data-target="#howtoTopupDialog">วิธีการเติมเงิน</button></div>
            </div>
            <hr>
            
            <div class="row justify-content-center">
                <?php 
                    include 'wallet.php';
                    showWallet();
                ?>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h6 class="mt-4" style="font-size: 12px;"><b>*จำนวนเงินฝาก</b></h6>
                    <input type="text" class="form-control" placeholder="ระบุจำนวนเงิน" id="amount">
                    <p class="mt-4" style="color: EB5757; font-size: 12px;"><b>**กรุณาโอนเงินเข้าบัญชีของเว็บก่อน ยืนยันการโอน**</b></p>
                    <p class="mt-4" style="font-size: 12px;"><b>* บัญชีธนาคาร</b></p>

                    <div class="rounded border">
                     <?php 
                        include 'bank_account.php';
                        for ($i = 0; $i < 1; $i++) {
                            showBankAccount('https://media.tmbbank.com/uploads/icon/img/s/5463_image_th.png', 'ธนาคารทหารไทย', 'ทดสอบ ทดสอบ', '485-226-8665');
                        }
                    ?>
                    </div> 

                    <p class="mt-4" style="font-size: 12px;"><b>* โปรโมชัน</b></p>
                    <div class="rounded border">
                        <div class="btn-group" role="group" style="width:100%">
                            <button type="button" id="promotionBtn" class="btn text-left" style="width: 90%">เลือกโปรโมชัน</button>
                            <button type="button" class="btn dropdown-toggle">
                        </div> 
                    </div>

                    <div class="row mt-4" id="promotionRow">
                        <div class="col-1">
                            <input type="radio" checked>
                        </div>
                        <div class="col-11" style="word-wrap: break-word;">
                            <div class="rounded" style="background-color: #7F7F7F;">
                            <span style="color: white;" id="promotionBlock" class="mt-2 ml-2 mr-2 mb-2">
                            <b></b>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center mt-4">
                <div class="col-md-8">
                    <div class="row justify-content-between">
                        <div class="col-6">
                            <button class="btn btn-outline-danger btn-block"><b>ยกเลิก</b></button>
                        </div>
                        <div class="col-6">
                            <button class="btn btn-success btn-block" id="confirmBtn" disabled><b>ยืนยัน</b></button>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
</body>
</html>

<script type="text/javascript">

$(document).ready(function() {
    $('#promotionRow').hide()
    $('#notifyDialog').modal('toggle');    

    $("#promotionBtn").click(function(event) {
        $('#selectDialog').modal('toggle');    
    });

    $("#confirmBtn").click(function(event) {
        $('#successDialog').modal('toggle');    
    });

    var isAmountEmpty = true;
    var isPromotionEmpty = true;

    $("a").click(function() {
        var value = $(this).attr('value'); 
        isPromotionEmpty = false
        $('#promotionBlock')[0].innerHTML = value
        $('#promotionRow').show()
        $('#confirmBtn').attr('disabled', (isAmountEmpty || isPromotionEmpty));
    });

    $('#amount').keyup(function () {
        isAmountEmpty = $(this).val() == ''
        $('#confirmBtn').attr('disabled', (isAmountEmpty || isPromotionEmpty));
    });
});
</script>