<?php 

function showWallet() {
    $money = 100000;
    echo '<div class="login-block text-center rounded col-md-8">
        <div class="row mt-2">
            <div class="col mt-4"><h6><b>ยอดคงเหลือในกระเป๋าเงิน</b></h6></div>
        </div>
        <div class="row justify-content-center mb-4">
        <b><span>THB &nbsp;<span style="font-size: 40px;">'.number_format($money, 2, ".", ",").'</span></span></b>
        </div>
    </div>';
}
?>