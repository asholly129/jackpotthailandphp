<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="index.css" rel="stylesheet">
        <!-- <link href="history_data.php" rel="stylesheet"> -->

        <title>Jackpot Thailand</title>
    </head>

    <body>
        <?php
        require 'history_data.php';
        include('nav.php'); 
        ?>

        <div class="container">
          <div class="row">
            <div class="col mt-4 mb-4">
              <a href="index.php" style="color:black"><span style="font-size: 25px;"><b>< ประวัติการทำรายการ</b></span></a>
            </div>
          </div>
        </div>


<div class="tab">
  <div class="row">
    <div class="col"><button type="submit" name="allButton" id="allButton" class="btn tablinks btn-block" onClick="selectType(event, 0)">ทั้งหมด</button></div>
    <div class="col"><button type="submit" name="topupButton" id="topupButton" class="btn tablinks btn-block" onClick="selectType(event, 1)">เติมเงิน</button></div>
    <div class="col"><button type="submit" name="withdrawButton" id="withdrawButton" class="btn tablinks btn-block" onClick="selectType(event, 3)">ถอนเงิน</button></div>
    <div class="col"><button type="submit" name="transferButton" id="transferButton" class="btn tablinks btn-block" onClick="selectType(event, 2)">โอนเงิน</button></div>
  </div>
</div>
        <div class="container">
           <div class="row">
             <div class="col">
              <div class="container">
                <div class="row justify-content-center mt-4">
                      <div id="0" class="tabcontent" style="width: 90%">
                        <?php 
                            $temps = array($temp1, $temp2, $temp3);
                            setupArrayAndaShowLists($temps);
                        ?>
                      </div>

                      <div id="1" class="tabcontent" style="width: 90%">
                        <?php 
                            $temps = array($temp1);
                            setupArrayAndaShowLists($temps);
                        ?>
                      </div>

                      <div id="2" class="tabcontent" style="width: 90%">
                      <?php 
                            $temps = array($temp3);
                            setupArrayAndaShowLists($temps);
                        ?>
                      </div>

                      <div id="3" class="tabcontent" style="width: 90%">
                      <?php 
                            $temps = array($temp2);
                            setupArrayAndaShowLists($temps);
                        ?>
                      </div>
                </div>
              </div>
              </div>

             </div>
           </div>
        </div>
    </body>
</html>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  selectType(event, 0);
  document.getElementById("allButton").click();
  $("#allButton").click(function(event) {
    selectType(event, 0);
  });
  $("#transferButton").click(function(event) {
    selectType(event, 2);
  });
  $("#topupButton").click(function(event) {
    selectType(event, 1);
  });
  $("#withdrawButton").click(function(event) {
    selectType(event, 3);
  });
});

function selectType(evt, id) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(id).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>