<?php 
    function showBankAccount($bankIconUrl, $bankName, $name, $bankNumber) {
        echo '
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="form-check mt-3 ml-2 mr-2 b-2">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                        <img src="'.$bankIconUrl.'" style="height: 30px; width: 30px;">
                            <b>'.$bankName.'</b>
                        </label>
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <div class="row justify-content-center">
                        <div class="grey-rounded-box center text-center">
                            <span style="font-size: 14px;">ชื่อบัญชี: <b>'.$name.'</b></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-2 mb-3">
                    <div class="row justify-content-center">
                        <div class="grey-rounded-box center text-center">
                            <span style="font-size: 14px;">เลขที่บัญชี: <b>'.$bankNumber.'</b></span>
                        </div>
                        
                    </div>
                </div>
            </div>';
    }
?>

<!-- <div style="width: 18px;"></div> -->
<!-- <img src="./icon/copy.png" width="18px">     -->
