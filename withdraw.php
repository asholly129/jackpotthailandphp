<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="index.css" rel="stylesheet">
        <link href="withdraw.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <title>Jackpot Thailand</title>
    </head>

    <body>
        <?php 
            include('nav.php'); 
            include 'dialog.php';
            $title = "ยืนยันถอนเงิน";
            $number = 10000;
            $name = "ทดสอบ ทดสอบ"; 
            $bankNumber = "485-226-8665";
            $bankName = "ธนาคารทหารไทย";
            openConfirmDialog($title, $number, $name, $bankNumber, $bankName)
        ?>

        <div class="container">
            <div class="row">
                <div class="col-7 mt-4">
                    <a href="index.php" style="color:black"><span style="font-size: 25px;"><b>< ถอนเงิน</b></span></a>
                </div>
                <div class="col-5 mt-4 text-right"><button class="btn btn-dark" data-toggle="modal" data-target="#howtoWithdrawDialog">วิธีการถอนเงิน</button></div>
            </div>
            <hr>
            
            <div class="row justify-content-center">
                <?php 
                    include 'wallet.php';
                    showWallet();
                ?>
            </div>

            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h6 class="mt-4">*จำนวนเงิน</h6>
                    <input type="text" class="form-control" placeholder="ระบุจำนวนเงิน 200 บาทขึ้นไป" id="amount">
                    <h6 class="mt-4">บัญชีรับเงินของสมาชิก</h6>
                    

                     <div class="rounded border">
                     <?php 
                        include 'bank_account.php';
                        for ($i = 0; $i < 1; $i++) {
                            showBankAccount('https://media.tmbbank.com/uploads/icon/img/s/5463_image_th.png', 'ธนาคารทหารไทย', 'ทดสอบ ทดสอบ', '485-226-8665');
                        }
                    ?>
                    </div> 
                </div>
            </div>

            <?php 
                $title = "ถอนเงินสำเร็จแล้ว";
                $description = "ระบบกำลังตรวจสอบและจะแจ้งผลการถอนเงิน ไปยังหน้ารายการถอนเงิน";
                openSuccessDialog($title, $description, null)
            ?>
            <div class="row justify-content-center mt-4">
                <div class="col-md-8">
                    <div class="row justify-content-between">
                        <div class="col-12 mb-2">
                            <button class="btn btn-success btn-block" data-toggle="modal" data-target="#confirmDialog" id="confirmBtn" disabled>ยืนยัน</button>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-outline-danger btn-block">ยกเลิก</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>

<script>
$(document).ready(function() {
    $('#amount').keyup(function () {
        var isAmountEmpty = $(this).val() == ''
        $('#confirmBtn').attr('disabled', (isAmountEmpty));
    });

});

</script>