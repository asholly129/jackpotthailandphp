<?php 

class Menu {
    public $name;
    public $icon;
}
$sport = new Menu();
$sport->name = "กีฬา";
$sport->icon = "icon/sport.png";

$lottery = new Menu();
$lottery->name = "หวย";
$lottery->icon = "icon/lottery.png";

$jackpot = new Menu();
$jackpot->name = "Jackpot";
$jackpot->icon = "icon/jackpot.png";

$casino = new Menu();
$casino->name = "คาสิโน";
$casino->icon = "icon/casino.png";

$games = new Menu();
$games->name = "เกมส์";
$games->icon = "icon/game.png";

$GLOBALS['menus'] = array($sport, $lottery, $jackpot, $casino, $games);

?>

<div id="smallSizeMenu" class="d-block d-sm-block d-md-block d-lg-none d-xl-none">
<div style="background-image: url(icon/redBG.png); height: 60px;">
<div class="scrollmenu">
<?php 
    foreach ($menus as $menu) {
        echo '
        <a href="#" style="width: 150px;">
            <img class="mt-2" src="'.$menu->icon.'" style="height: 25px;">
            <h6 class="mt-1 mb-2"><b>'.$menu->name.'</b></h6>
        </a>
        ';  
    }
?>
</div>
</div>    
</div>