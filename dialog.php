

<div class="modal fade" id="howtoWithdrawDialog" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
        <div class="modal-title"><h4>วิธีการถอนเงิน</h4></div>
        </div>
        <div class="modal-body">
            <p>1. ระบุจำนวนเงิน<br>2. เลือกเว็บไซต์ที่ต้องการโอน<br>3. ยืนยันการโอน</p>
        </div>
        <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-outline-dark" style="width: 50%;" data-dismiss="modal">ปิด</button>
        </div>
    </div>
</div>
</div>


<?php 
    function openConfirmDialog($title, $number, $name, $bankNumber, $bankName) {
        echo '
        <div class="modal fade" id="confirmDialog" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                <div class="modal-title"><h4 style="color: #E1B643;"><b>'.$title.'</b></h4></div>
                </div>
                <div class="modal-body">
        
                <div class="row justify-content-center mb-2">
                    <div class="col-5 text-center">
                        <div class="yellow-box"><b>จำนวน</b></div>
                    </div>
                    <div class="col-7"><b>'.number_format($number, 2, ".", ",").' THB</b></div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-5 text-center">
                    <div class="yellow-box"><b>บัญชี</b></div>
                    </div>
                    <div class="col-7"><b>'.$name.'<br>'.$bankNumber.'<br>'.$bankName.'</b></div>
                </div>
                </div>
                <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-outline-danger" style="width: 45%;" data-dismiss="modal"><b>ยกเลิก</b></button>
                <button type="button" class="btn" style="width: 45%; background-color: #2FA84F; color: white;" data-toggle="modal" data-dismiss="modal" data-target="#successDialog"><b>ยืนยัน</b></button>
                </div>
            </div>
        </div>
        </div>
        ';
    }

    function confirmDialog($title, $number, $from, $to) {
        echo '
            <div class="modal fade" id="confirmTransferDialog" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <div class="modal-title"><h4 style="color: #E1B643;"><b>'.$title.'</b></h4></div>
                    </div>
                    <div class="modal-body">
            
                    <div class="row justify-content-center mb-2">
                        <div class="col-5 text-center">
                            <div class="yellow-box"><b>จำนวน</b></div>
                        </div>
                        <div class="col-7"><b>'.number_format($number, 2, ".", ",").' THB</b></div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-5 text-center">
                        <div class="yellow-box"><b>จาก</b></div>
                        </div>
                        <div class="col-7"><b>'.$from.'</div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-5 text-center">
                        <div class="yellow-box"><b>ถึง</b></div>
                        </div>
                        <div class="col-7"><b>'.$to.'</div>
                    </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-outline-danger" style="width: 45%;" data-dismiss="modal"><b>ยกเลิก</b></button>
                    <button type="button" id="confirmTransferBtn" class="btn" style="width: 45%; background-color: #2FA84F; color: white;"><b>ยืนยัน</b></button>
                    </div>
                </div>
            </div>
            </div>
        ';
    }
    
?>

<?php 
    function openSuccessDialog($title, $description, $linkUrl) {
        $urlBtn = '<button type="button" class="btn btn-primary" id="urlButton" style="width: 45%;" data-dismiss="modal" url='.$linkUrl.'>ไปยังเว็บไซต์</button>';
        echo '
            <div class="modal fade" id="successDialog" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <div class="modal-title"><h4>'.$title.'</h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                            <svg width="81" height="81" viewBox="0 0 81 81" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M40.5 6.75C21.87 6.75 6.75 21.87 6.75 40.5C6.75 59.13 21.87 74.25 40.5 74.25C59.13 74.25 74.25 59.13 74.25 40.5C74.25 21.87 59.13 6.75 40.5 6.75ZM33.75 57.375L16.875 40.5L21.6338 35.7413L33.75 47.8237L59.3663 22.2075L64.125 27L33.75 57.375Z" fill="#2FA84F"/>
                            </svg>
                        </div>
                        <div class="row justify-content-center text-center ml-4 mr-4 mb-2 mt-2">
                            <span><b>'.$description.'</b></span>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-outline-dark" style="width: 45%;" data-dismiss="modal">ปิด</button>';
                    if ($linkUrl != null) {
                        echo $urlBtn;
                    }
                    echo '</div>
                </div>
            </div>
            </div>
            ';
    }

    function openFailDialog($title, $description) {
        echo '
            <div class="modal fade" id="failDialog" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <div class="modal-title"><h4>'.$title.'</h4></div>
                    </div>
                    <div class="modal-body">
                        <div class="row justify-content-center">
                        <svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M37.5 6.25C20.25 6.25 6.25 20.25 6.25 37.5C6.25 54.75 20.25 68.75 37.5 68.75C54.75 68.75 68.75 54.75 68.75 37.5C68.75 20.25 54.75 6.25 37.5 6.25ZM40.625 53.125H34.375V46.875H40.625V53.125ZM40.625 40.625H34.375V21.875H40.625V40.625Z" fill="#EB5757"/>
                        </svg>                        
                        </div>
                        <div class="row justify-content-center text-center ml-4 mr-4 mb-2 mt-2">
                            <h2><b>ไม่สามารถโอนเงินได้</b></h2>
                            <span><b>'.$description.'</b></span>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-outline-danger" style="width: 45%;" data-dismiss="modal">ยกเลิก</button>
                    <button type="button" class="btn btn-primary" id="urlButton" style="width: 45%;" data-dismiss="modal" onclick=window.location.href="topup.php">เติมเงิน ตอนนี้</button>
                    </div>
                </div>
            </div>
            </div>
            ';
    }


    function showNotifyDialog() { 
        echo '
            <div class="modal fade" id="notifyDialog" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                    <div class="modal-title"><h4><b>แจ้งเตือน</b></h4></div>
                    </div>
                    <div class="modal-body">
                        <p><b>*ลูกค้าต้องฝากเงินด้วยบัญชีของลูกค้าเท่านั้น หากใช้บัญชีอื่นโอนมา ระบบจะไม่เติมเครดิตให้</b></p><br>
                        <p style="color: #EB5757"><b>**กรุณาโอนเงินเข้าบัญชีของเว็บก่อนทำการยืนยัน การโอน หากไม่ทำตามขั้นตอนระบบจะไม่เติมเครดิตให้</b></p>
            
                    </div>
                    <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-outline-dark" style="width: 50%;" data-dismiss="modal">ปิด</button>
                    </div>
                </div>
            </div>
            </div>  
        ';
    }

    function openSelectDialog($lists) {
        echo '
            <div class="modal fade" id="selectDialog" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="list-group">';
                        
                        foreach ($lists as $list) {
                            echo '<a href="#" class="list-group-item list-group-item-action" data-dismiss="modal" value='.$list.'>'.$list.'</a>';
                        }

                        echo '</div>
                    </div>
                </div>
            </div>
            </div>    
        ';
    }
    
?>
