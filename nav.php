<html>
<head>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>


<div class="modal fade" id="logoutDialog" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
        <div class="modal-title"><h4><b>ยืนยันออกจากระบบ</b></h4></div>
        </div>
        <div class="modal-body">
        <div class="row justify-content-center">
        <svg width="75" height="75" viewBox="0 0 75 75" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M37.5 6.25C20.25 6.25 6.25 20.25 6.25 37.5C6.25 54.75 20.25 68.75 37.5 68.75C54.75 68.75 68.75 54.75 68.75 37.5C68.75 20.25 54.75 6.25 37.5 6.25ZM40.625 53.125H34.375V46.875H40.625V53.125ZM40.625 40.625H34.375V21.875H40.625V40.625Z" fill="#EB5757"/>
        </svg>                        
        </div>
        <div class="row justify-content-center text-center ml-4 mr-4 mb-2 mt-2">
            <h5><b>คุณยืนยันออกจากระบบนี้ใช่หรือไม่ ?</b></h5>
        </div>
        </div>
        <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-outline-danger btn-lg" style="width: 45%;" data-dismiss="modal"><b>ยกเลิก</b></button>
        <button type="button" class="btn btn-danger btn-lg" style="width: 45%;" data-dismiss="modal"><b>ยืนยัน</b></button>
        </div>
    </div>
</div>
</div>

<div id="mySidenav" class="sidenav" style="height: 100%">
    <img src="icon/logo.png" class="ml-4" style="width: 145px;"></span>
  
  <a class="mt-2" href="index.php" style="color: black; font-size: 22px;"><b>หน้าแรก</b></a>
  <a href="#" style="color: black; font-size: 22px;"><b>ติดต่อเรา</b></a>

  <?php 
    if ($isLogin == true) {
        echo '
            <div class="row justify-content-center mb-2 position-absolute fixed-bottom">
                <button class="btn btn-danger rounded btn-block btn-lg" style="width: 80%;" data-toggle="modal" data-target="#logoutDialog">ออกจากระบบ</button>
            </div>      
        ';
    }
  ?>    
</div>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">
      <img src="icon/logo.png" style="width: 145px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#" onclick="openNav()">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse justify-content-end" id="collapsingNavbarXs">
        <ul class="navbar-nav" style="font-size: 15px;">
            <li class="nav-item" style="width: 100px;">
                <div class="row justify-content-center">
                <a class="nav-link" href="index.php">
                    <span class="align-bottom">
                        <span><img src="icon/home.png" style="height: 15px;"></span>
                        <span class="ml-2"><b>หน้าแรก</b></span>
                    </span>
                </a>
                </div>
            </li>

            <li class="nav-item" style="width: 100px;">
                <div class="row justify-content-center">
                <a class="nav-link" href="index.php">
                    <span class="align-bottom">
                        <span><img src="icon/sport.png" style="height: 15px;"></span>
                        <span class="ml-2"><b>กีฬา</b></span>
                    </span>
                </a>
                </div>
            </li>

            <li class="nav-item" style="width: 100px;">
                <div class="row justify-content-center">
                <a class="nav-link" href="index.php">
                    <span class="align-bottom">
                        <span><img src="icon/lottery.png" style="height: 15px;"></span>
                        <span class="ml-2"><b>หวย</b></span>
                    </span>
                </a>
                </div>
            </li>

            <li class="nav-item" style="width: 100px">
                <div class="row justify-content-center">
                <a class="nav-link" href="index.php">
                    <span class="align-bottom">
                        <span><img src="icon/jackpot.png" style="height: 15px;"></span>
                        <span class="ml-2"><b>Jackpot</b></span>
                    </span>
                </a></div>
            </li>

            <li class="nav-item" style="width: 100px">
                <div class="row justify-content-center">
                <a class="nav-link" href="index.php">
                    <span class="align-bottom">
                        <span><img src="icon/casino.png" style="height: 15px;"></span>
                        <span class="ml-2"><b>คาสิโน</b></span>
                    </span>
                </a></div>
            </li>

            <li class="nav-item" style="width: 100px">
                <div class="row justify-content-center">
                <a class="nav-link" href="index.php">
                    <span class="align-bottom">
                        <span><img src="icon/game.png" style="height: 15px;"></span>
                        <span class="ml-2"><b>เกมส์</b></span>
                    </span>
                </a></div>
            </li>

            <li class="nav-item">
            <button class="btn" style="color: white; background-color: #E1B643"><b>ติดต่อเรา</b></button>    
            </li>
            
            <?php 
                $username = 'ufadb40002';
                if ($isLogin == true) {
                    echo '<li class="nav-item">
                        <a class="nav-link" href="#"><b>'.$username.'</b></a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="#"><img src="icon/logout.png" data-toggle="modal" data-target="#logoutDialog" style="height: 30px;">
                            </a>
                    </li>';
                }
            ?>
        </ul>
    </div>
</nav>
    
</body>
</html>

<script>

$(document).ready(function() { 
    var width = $(window).width();
    console.log('width: ' + width);

    $(window).resize(function(data) {
        console.log(data.target.innerWidth)
    });
});
var isOpen = false
function openNav() {
    isOpen = !isOpen
    if (isOpen == true) {
        document.getElementById("mySidenav").style.width = "300px";
    } else {
        document.getElementById("mySidenav").style.width = "00px";
    }
}

/* Set the width of the side navigation to 0 */
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>